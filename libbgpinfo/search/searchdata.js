var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuw~",
  1: "adfhprst",
  2: "bs",
  3: "abcdflpqru",
  4: "abcdefghilmnoprstw~",
  5: "_abcdefiklmnoprst",
  6: "pr",
  7: "ps",
  8: "adi"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Friends",
  8: "Macros"
};

